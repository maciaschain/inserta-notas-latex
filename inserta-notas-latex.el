;;; -*- lexical-binding: t -*-

;; Copyright (C) 2020  Juan Manuel Macías

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Code:

(require 'button)

  (defface nota
    '((t :foreground "chocolate"))
    "")

(defun botones-notas ()
  "Genera enlaces internos para cada marca de nota, apuntando a
su correspondiente definición"
  (interactive)
  (let
      ((x (make-marker))
       (y (make-marker)))
    (save-excursion
      (goto-char (point-min))
      (while
	  (re-search-forward "\\\\nota{" nil t)
	(set-marker x (- (point) 6))
	(re-search-forward "}" nil t)
	(set-marker y (point))
	(save-restriction
	  (narrow-to-region x y)
	  (string-match "\\\\nota{\\(.+\\)}" (buffer-string))
	  (let
	      ((num (match-string 1 (buffer-string))))
	    (goto-char (point-min))
	    (insert-button (concat "\\nota{" num "}")
			   'action (lambda (x)
				     (re-search-forward (concat "\\\\notadef{" num "}{")))
			   'face 'nota)
	    (set-mark-command nil)
	    (goto-char (point-max))
	    (delete-active-region)
	    (deactivate-mark)))))))

(defun botones-def ()
  "Genera enlaces internos para cada definición de nota, apuntando a
su correspondiente marca en el texto"
  (interactive)
  (let
      ((x (make-marker))
       (y (make-marker)))
    (save-excursion
      (goto-char (point-min))
      (while
	  (re-search-forward "\\\\notadef{" nil t)
	(set-marker x (- (point) 9))
	(re-search-forward "}" nil t)
	(set-marker y (point))
	(save-restriction
	  (narrow-to-region x y)
	  (string-match "\\\\notadef{\\(.+\\)}" (buffer-string))
	  (let
	      ((num (match-string 1 (buffer-string))))
	    (goto-char (point-min))
	    (insert-button (concat "\\notadef{" num "}")
			   'action (lambda (x)
				     (re-search-backward (concat "\\\\nota{" num "}")))
			   'face 'nota)
	    (set-mark-command nil)
	    (goto-char (point-max))
	    (delete-active-region)
	    (deactivate-mark)))))))

(defun activar-enlaces-notas ()
  "Genera los enlaces internos para las marcas y las definiciones
de las notas"
  (interactive)
  (botones-notas)
  (botones-def))

(defun inserta-nota-def ()
  "Inserta una definición de nota para una marca dada"
  (re-search-forward "\\\\end{document}" nil t)
  (beginning-of-line)
  (insert (concat "\n" "\\notadef{" (format "%s" (number-to-string contador-nota)) "}" "{}" "\n\n"))
  (re-search-backward "{")
  (forward-char 1))

(defun inserta-nota-latex ()
  "Inserta la nota numerada y su definición, y posiciona el
cursor en ésta última para comenzar a escribir la nota. A su vez,
también genera un enlace entre ambas"
  (interactive)
  (if
      (save-excursion
	(goto-char (point-max))
	(re-search-backward "\\\\notadef{" nil t))
      (progn
	(save-excursion
	  (goto-char (point-max))
	  (re-search-backward "\\\\notadef{" nil t)
	  (re-search-forward "{" nil t)
	  (set-mark-command nil)
	  (re-search-forward "}" nil t)
	  (forward-char -1)
	  (save-restriction
	    (narrow-to-region (region-beginning) (region-end))
	    (setq contador-nota (string-to-number (buffer-string)))
	    (deactivate-mark)))
	(insert (concat "\\nota{" (format "%s" (number-to-string (setf contador-nota (+ contador-nota 1)))) "}"))
	(inserta-nota-def)
	(activar-enlaces-notas))
    (setq contador-nota 0)
    (insert (concat "\\nota{" (format "%s" (number-to-string (setf contador-nota (+ contador-nota 1)))) "}"))
    (inserta-nota-def)
    (activar-enlaces-notas)
    (save-excursion
      (goto-char (point-min))
      (unless (re-search-forward "eval: (activar-enlaces-notas)" nil t)
	(add-file-local-variable 'eval '(activar-enlaces-notas))))))

(defun convierte-notas-latex ()
  "Convierte todas las notas estándar de un documento de LaTeX,
es decir, las introducidas con la macro `\footnote' en notas
separadas, con la marca en el texto y las defiiciones al pie,
todas convenientemente numeradas"
  (interactive)
  (setq contador-nota 0)
  (let
      ((x (make-marker))
       (y (make-marker)))
    (save-excursion
      (goto-char (point-min))
      (while
	  (re-search-forward "\\\\footnote{" nil t)
	(set-marker x (- (point) 10))
	(sp-end-of-next-sexp)
	(set-marker y (+ (point) 1))
	(save-restriction
	  (narrow-to-region x y)
	  (goto-char (point-min))
	  (re-search-forward "\\\\footnote{" nil t)
	  (set-mark-command nil)
	  (sp-end-of-sexp)
	  (kill-region (region-beginning) (region-end))
	  (deactivate-mark))
	(delete-region x y)
	(save-excursion
	  (insert (concat "\\nota{" (format "%s" (number-to-string (setf contador-nota (+ contador-nota 1)))) "}"))
	  (re-search-forward "\\\\end{document}" nil t)
	  (beginning-of-line)
	  (insert (concat "\n" "\\notadef{" (format "%s" (number-to-string contador-nota)) "}" "{" (car kill-ring) "}" "\n\n"))
	  (re-search-backward "{")
	  (forward-char 1))))
    (activar-enlaces-notas)
    (save-excursion
      (goto-char (point-min))
      (unless (re-search-forward "eval: (activar-enlaces-notas)" nil t)
	(add-file-local-variable 'eval '(activar-enlaces-notas))))))

(defun elimina-notas-latex ()
"Elimina una nota (marca y definición) tras indicar su número"
  (interactive)
  (let
      ((numnota (read-from-minibuffer "Eliminar nota núm.: ")))
    (save-excursion
      (goto-char (point-min))
      (re-search-forward (concat "\\\\nota{" numnota "}") nil t)
      (re-search-backward "\\\\" nil t)
      (set-mark-command nil)
      (re-search-forward "}" nil t)
      (delete-region (region-beginning) (region-end))
      (deactivate-mark)
      (re-search-forward (concat "\\\\notadef{" numnota "}") nil t)
      (if (save-excursion
	    (re-search-forward "\\\\notadef{" nil t))
	  (progn
	    (re-search-backward "\\\\" nil t)
	    (set-mark-command nil)
	    (forward-char 1)
	    (re-search-forward "\\\\notadef{" nil t)
	    (re-search-backward "\\\\" nil t)
	    (save-restriction
	      (narrow-to-region (region-beginning) (region-end))
	      (goto-char (point-min))
	      (while (re-search-forward ".+" nil t)
		(replace-match "" t nil)))
	    (deactivate-mark))
	(re-search-backward "\\\\" nil t)
	(set-mark-command nil)
	(forward-char 1)
	(re-search-forward "\\\\end{document}" nil t)
	(re-search-backward "\\\\" nil t)
	(save-restriction
	  (narrow-to-region (region-beginning) (region-end))
	  (goto-char (point-min))
	  (while (re-search-forward ".+" nil t)
	    (replace-match "" t nil)))
	(deactivate-mark)))))
